import bos.GameBoard;
import bos.GamePiece;
import bos.MoveRandomly;
import bos.RelativeMove;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by 44918054 on 12/09/2017.
 */
public class Bee extends BeeComponent implements GamePiece<Cell>{

    Cell location;

    public Bee(Cell location){
        this.location = location;
    }

    public void paint(Graphics g){
        g.setColor(Color.pink);
        g.fillOval(location.x + location.width / 4, location.y + location.height / 4, location.width / 2, location.height / 2);

    }

    public Boolean sting(ArrayList<Character> c){
        for(int i =0; i<c.size(); i ++) {
            if (c.get(i).location == location) {
                return true;
            }
        }
        return false;
    }


    @Override
    public Cell getLocationOf() {
        return location;
    }

    @Override
    public void setLocationOf(Cell cell) {
        location = cell;

    }

    public RelativeMove moveRand(Stage stage){
        return new MoveRandomly(stage.grid, this);
    }
}
