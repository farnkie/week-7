import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by 44918054 on 12/09/2017.
 */
public class BeeSwarm extends BeeComponent{
    public LinkedList<BeeComponent> bees = new LinkedList<BeeComponent>();

    public BeeSwarm(BeeComponent bee, BeeComponent bee2){
        bees.add(bee);
        bees.add(bee2);
    }

    public void add(BeeComponent bee){
        bees.add(bee);
    }

    public Boolean sting(ArrayList<Character> c){
        Iterator<BeeComponent> list = bees.iterator();
        while(list.hasNext()){
            BeeComponent bee = list.next();
            if(bee.sting(c))
                return true;
        }
        return false;
    }


}
