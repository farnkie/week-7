import bos.MoveRandomly;
import bos.RelativeMove;

import java.awt.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class Stage extends KeyObservable {
    protected Grid grid;
    protected Character sheep;
    protected Character shepherd;
    protected Character wolf;
    protected Player player;
    protected Bee bee;
    private List<Character> allCharacters;

    private Instant timeOfLastMove = Instant.now();
    private List<RelativeMove> moves;

    public Stage() {
        grid     = new Grid(10, 10);
        shepherd = new Shepherd(grid.getRandomCell(), new StandStill());
        sheep    = new Sheep(grid.getRandomCell(), new MoveTowards(shepherd));
        wolf     = new Wolf(grid.getRandomCell(), new MoveTowards(sheep));
        player = new Player(grid.getRandomCell());
        bee = new Bee(grid.getRandomCell());
        this.register(player);

        allCharacters = new ArrayList<Character>();
        allCharacters.add(sheep);
        allCharacters.add(shepherd);
        allCharacters.add(wolf);

    }

    public void update(){
        if(!player.inMove()) {

            if (sheep.location == shepherd.location) {
                System.out.println("Sheep is safe!");
                System.exit(0);
            } else if (sheep.location == wolf.location) {
                System.out.println("Sheep is dead!");
                System.exit(1);
            } else {
                if (sheep.location.x == sheep.location.y) {
                    sheep.setBehaviour(new StandStill());
                }
                timeOfLastMove = Instant.now();
                sheep.aiMove(this).perform();
                wolf.aiMove(this).perform();
                shepherd.aiMove(this).perform();
                bee.moveRand(this).perform();
                player.startMove();

            }


        }
    }

    public void paint(Graphics g, Point mouseLocation) {
        grid.paint(g, mouseLocation);
        sheep.paint(g);
        shepherd.paint(g);
        wolf.paint(g);
        player.paint(g);
        bee.paint(g);
    }


}