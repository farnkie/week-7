import java.awt.*;
import java.util.Optional;

public class Wolf extends Character {

    Behaviour behaviour;

    public Wolf(Cell location, Behaviour behaviour) {
        super(location, behaviour);
        display = Optional.of(Color.RED);
    }


}